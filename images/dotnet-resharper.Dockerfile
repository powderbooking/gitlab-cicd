ARG DOTNET_VERSION=8.0

# ref: https://mcr.microsoft.com/en-us/product/dotnet/sdk/tags
FROM mcr.microsoft.com/dotnet/sdk:${DOTNET_VERSION}

# ref: https://www.jetbrains.com/help/resharper/ReSharper_Command_Line_Tools.html
ARG RESHARPER_VERSION=2024.1.3
# ref: https://gitlab.com/gitlab-org/cli/-/releases
ARG GLAB_VERSION=1.42.0

RUN apt-get update && apt-get install -y curl unzip tar wget git

RUN curl -L https://download-cdn.jetbrains.com/resharper/dotUltimate.${RESHARPER_VERSION}/JetBrains.ReSharper.CommandLineTools.${RESHARPER_VERSION}.zip -o /tmp/resharper.zip \
    && unzip -qd /opt/resharper /tmp/resharper.zip

RUN curl -L https://gitlab.com/ignis-build/sarif-converter/-/releases/permalink/latest/downloads/bin/sarif-converter-linux -o /usr/local/bin/sarif-converter \
    && chmod +x /usr/local/bin/sarif-converter

RUN curl -L https://gitlab.com/gitlab-org/cli/-/releases/v${GLAB_VERSION}/downloads/glab_${GLAB_VERSION}_Linux_x86_64.tar.gz | tar zxv -C /tmp \
    && mv /tmp/bin/glab /usr/local/bin/glab \
    && chmod +x /usr/local/bin/glab 

RUN cat /opt/resharper/inspectcode.sh | sed -E 's/D=.+/D=\/opt\/resharper/g' > inspectcode.sh \
    && mv inspectcode.sh /opt/resharper/inspectcode.sh

RUN cat /opt/resharper/cleanupcode.sh | sed -E 's/D=.+/D=\/opt\/resharper/g' > cleanupcode.sh \
    && mv cleanupcode.sh /opt/resharper/cleanupcode.sh

RUN chmod +x /opt/resharper/inspectcode.sh
RUN chmod +x /opt/resharper/cleanupcode.sh
RUN ln -s /opt/resharper/inspectcode.sh /usr/bin/inspectcode
RUN ln -s /opt/resharper/cleanupcode.sh /usr/bin/cleanupcode

RUN apt-get remove -y curl unzip wget \
    && apt-get clean \
    && apt-get autoremove -y --purge \
    && rm -rf /tmp/*