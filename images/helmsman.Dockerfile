# ref: https://github.com/Praqma/helmsman/blob/master/Dockerfile
# they upload either latest, or sometimes specific versions
# I want to keep control over the versions of certain dependencies (e.g. kube, helm)
# slightly adapted their original dockerfile, not building from source but downloading instead
ARG ALPINE_VERSION="3.20"
# ref: https://kubernetes.io/releases/
ARG GLOBAL_KUBE_VERSION="v1.30.2"
# ref: https://github.com/helm/helm/releases
ARG GLOBAL_HELM_VERSION="v3.15.2"
# ref: https://github.com/databus23/helm-diff/releases
ARG GLOBAL_HELM_DIFF_VERSION="v3.9.8"
ARG GLOBAL_HELM_GCS_VERSION="0.4.2"
ARG GLOBAL_HELM_S3_VERSION="v0.16.0"
# ref: https://github.com/jkroepke/helm-secrets/releases
ARG GLOBAL_HELM_SECRETS_VERSION="v4.6.0"
# ref: https://github.com/getsops/sops/releases
ARG GLOBAL_SOPS_VERSION="v3.8.1"
# ref: https://github.com/mikefarah/yq/releases
ARG GLOBAL_YQ_VERSION="4.44.2"
# ref: https://github.com/Praqma/helmsman/releases
ARG GLOBAL_HELMSMAN_VERSION="3.17.0"

### Helm Installer ###
FROM alpine:${ALPINE_VERSION} AS helm-installer
ARG GLOBAL_KUBE_VERSION
ARG GLOBAL_HELM_VERSION
ARG GLOBAL_HELM_DIFF_VERSION
ARG GLOBAL_HELM_GCS_VERSION
ARG GLOBAL_HELM_S3_VERSION
ARG GLOBAL_HELM_SECRETS_VERSION
ARG GLOBAL_SOPS_VERSION
ARG GLOBAL_YQ_VERSION
ARG GLOBAL_HELMSMAN_VERSION
ENV YQ_VERSION=${GLOBAL_YQ_VERSION}
ENV HELMSMAN_VERSION=${GLOBAL_HELMSMAN_VERSION}
ENV KUBE_VERSION=$GLOBAL_KUBE_VERSION
ENV HELM_VERSION=$GLOBAL_HELM_VERSION
ENV HELM_DIFF_VERSION=$GLOBAL_HELM_DIFF_VERSION
ENV HELM_GCS_VERSION=$GLOBAL_HELM_GCS_VERSION
ENV HELM_S3_VERSION=$GLOBAL_HELM_S3_VERSION
ENV HELM_SECRETS_VERSION=$GLOBAL_HELM_SECRETS_VERSION
ENV SOPS_VERSION=$GLOBAL_SOPS_VERSION
ENV HELM_DIFF_THREE_WAY_MERGE=true

RUN apk add --update --no-cache ca-certificates git openssh-client openssl ruby curl wget tar gzip bash

RUN curl -L https://github.com/getsops/sops/releases/download/${SOPS_VERSION}/sops-${SOPS_VERSION}.linux.amd64 -o /usr/local/bin/sops \
    && chmod +x /usr/local/bin/sops

RUN curl --retry 5 -L https://storage.googleapis.com/kubernetes-release/release/${KUBE_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl

RUN curl --retry 5 -Lk https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz | tar zxv -C /tmp \
    && mv /tmp/linux-amd64/helm /usr/local/bin/helm && rm -rf /tmp/linux-amd64 \
    && chmod +x /usr/local/bin/helm

RUN helm plugin install https://github.com/hypnoglow/helm-s3.git --version ${HELM_S3_VERSION}
RUN helm plugin install https://github.com/nouney/helm-gcs --version ${HELM_GCS_VERSION}
RUN helm plugin install https://github.com/databus23/helm-diff --version ${HELM_DIFF_VERSION}
RUN helm plugin install https://github.com/jkroepke/helm-secrets --version ${HELM_SECRETS_VERSION}

RUN curl -L https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_linux_amd64 -o /usr/local/bin/yq \
    && chmod +x /usr/local/bin/yq

RUN curl -L https://github.com/Praqma/helmsman/releases/download/v${HELMSMAN_VERSION}/helmsman_${HELMSMAN_VERSION}_linux_amd64.tar.gz | tar zxv -C /tmp \
    && mv /tmp/helmsman /usr/local/bin/helmsman \
    && chmod +x /usr/local/bin/helmsman

### Final Image ###
FROM alpine:${ALPINE_VERSION} AS base

RUN apk add --update --no-cache ca-certificates git openssh-client ruby curl bash gnupg gcompat
RUN gem install hiera-eyaml hiera-eyaml-gkms --no-doc
RUN update-ca-certificates

COPY --from=helm-installer /usr/local/bin/kubectl /usr/local/bin/kubectl
COPY --from=helm-installer /usr/local/bin/helm /usr/local/bin/helm
COPY --from=helm-installer /usr/local/bin/sops /usr/local/bin/sops
COPY --from=helm-installer /root/.cache/helm/plugins/ /root/.cache/helm/plugins/
COPY --from=helm-installer /root/.local/share/helm/plugins/ /root/.local/share/helm/plugins/
COPY --from=helm-installer /usr/local/bin/yq /usr/local/bin/yq
COPY --from=helm-installer /usr/local/bin/helmsman /usr/local/bin/helmsman
