ARG ALPINE_VERSION="3.20"
# ref: https://hub.docker.com/_/python
ARG PYTHON_VERSION="3.12.4"
# ref: https://github.com/python-poetry/poetry/releases
ARG POETRY_VERSION="1.8.3"

FROM python:${PYTHON_VERSION}-alpine${ALPINE_VERSION} AS python-base
ARG POETRY_VERSION
# ref: https://github.com/ufoscout/docker-compose-wait
ARG WAIT_VERSION=2.12.1


# set path for poetry: https://stackoverflow.com/a/61751745
# example using poetry https://github.com/wemake-services/wemake-django-template/blob/master/%7B%7Bcookiecutter.project_name%7D%7D/docker/django/Dockerfile
# https://github.com/orgs/python-poetry/discussions/1879#discussioncomment-216865
ENV \
    # python:
    # dump the traceback on error
    PYTHONFAULTHANDLER=1 \
    # write messages directly to stdout instead of buffering
    PYTHONUNBUFFERED=1 \
    # prevents python creating .pyc files
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONHASHSEED=random \
    # pip:
    PIP_NO_CACHE_DIR=1 \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    # poetry:
    POETRY_HOME="/usr/local/poetry" \
    POETRY_VIRTUALENVS_IN_PROJECT=1 \
    # do not ask any interactive question
    POETRY_NO_INTERACTION=1 \
    POETRY_CACHE_DIR='/var/cache/pypoetry' \
    # paths where everything gets installed
    PYSETUP_PATH="/usr/local/pysetup" \
    VENV_PATH="/usr/local/pysetup/.venv"

# prepend src, poetry and venv to path
ENV PATH="$SRC_PATH/:$POETRY_HOME/bin:$VENV_PATH/bin:$PATH"

# install runtime dependencies
# https://github.com/ufoscout/docker-compose-wait
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/${WAIT_VERSION}/wait /wait
RUN chmod +x /wait

RUN apk add --update curl && rm -rf /var/cache/apk/*

# install poetry
RUN curl -sSL https://install.python-poetry.org | python3 - --version ${POETRY_VERSION}
