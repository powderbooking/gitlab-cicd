ARG ALPINE_VERSION=3.20.0

FROM alpine:${ALPINE_VERSION}

# ref: https://github.com/mikefarah/yq/releases
ARG YQ_VERSION=4.44.2
# ref: https://gitlab.com/gitlab-org/cli/-/releases
ARG GLAB_VERSION=1.42.0

RUN apk fix && \
    apk --no-cache --update add git git-lfs gpg less openssh patch perl curl wget tar gzip && \
    git lfs install

RUN curl -L https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_linux_amd64 -o /usr/local/bin/yq \
    && chmod +x /usr/local/bin/yq

RUN curl -L https://gitlab.com/gitlab-org/cli/-/releases/v${GLAB_VERSION}/downloads/glab_${GLAB_VERSION}_Linux_x86_64.tar.gz | tar zxv -C /tmp \
    && mv /tmp/bin/glab /usr/local/bin/glab \
    && chmod +x /usr/local/bin/glab 

# cleanup
RUN rm -rf /tmp/*